FROM python:3.10

ENV PYTHONUNBUFFERED 1

WORKDIR /home/usuario/app

COPY . .

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8000

CMD [ "python3", "manage.py", "runserver"]
