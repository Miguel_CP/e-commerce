# e-commerce

## Introducción
En esta tarea, tendrás que implementar un backend de Django utilizando una arquitectura de microservicio en AWS. Esta tarea está destinada a evaluar tu comprensión de Django, el diseño de API RESTful y las buenas prácticas de codificación.

## Descripción
Tendrás que crear un backend de Django para una plataforma de comercio electrónico simple. La plataforma debe admitir el registro de usuarios, la autenticación y la gestión de productos. Sin embargo, para acortar este ejercicio, supondremos que se ha completado todo el código de registro y autenticación. Solo te centrarás en la parte de gestión de productos (APIs y pruebas).
Te proporcionaremos una implementación mínima de código como guía. Agrega o elimina cualquier campo en los modelos según lo requiera en su implementación. Corrija los errores si los encuentra. Esto es solo una guia.
Solo se requiere una API para este ejercicio, como se describe a continuación.
Establece un temporizador de 5 horas cuando hagas el ejercicio y detente ahí. Utiliza los últimos minutos para explicar en un archivo cómo procederías para completar lo que queda.

Servicio de Gestión de Producto
```
/models.py

from django.db import models

class Product(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)

class Review(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    grade = models.DecimalField(max_digits=1, decimal_places=1)
```

API:

/products/: “list” (accesible por cualquier usuario) y “create” (accessible solo por usuarios registrados)

Al enumerar productos, debe haber un campo que de la información sobre las reseñas de productos:
Nota media
Número de reseñas
Reseña del producto por parte del usuario que hace la petición

## Criterios de evaluación
Implementación adecuada de los modelos, vistas y serializadores de Django.
Uso adecuado de los principios de diseño de la API RESTful.
Cobertura de pruebas unitarias y diseño de casos de prueba. (unicamente la funcion assertEqual está permitida)
Calidad del código.
Uso adecuado de Git para control de versiones y gestión de proyectos.
(Puntos de bonificación) Dockerización e implementación en AWS.

# Ejecución del ejercicio 

## Rutas de la aplicación
- products/: Esta ruta muestra una lista de todos los productos disponibles y permite a los usuarios crear nuevos productos.

- products/<int:pk>/: Esta ruta muestra los detalles de un producto en particular.

- products/<int:product_id>/reviews/: Esta ruta muestra una lista de reseñas de un producto en particular. Los usuarios pueden crear nuevas reseñas para el producto.
- admin/: Esta ruta se usa para hacer login con los usuarios, si el usuario es administrador podrá acceder al CRUD de los modelos.

## Sin docker
El ejercicio ha sido desarrollado en un entorno con:
- Python 3.10.6
- Django 4.2

Se recomienda usar dichas versiones para asegurarse el correcto funcionamiento.

Teniendo Python, Django y git instalado seguir los siguientes pasos:
1. clonar el repositorio
    ```
    git clone https://gitlab.com/Miguel_CP/e-commerce.git
    ```
2. posicionarse dentro del repositorio generado:
    ```
    cd e-commerce
    ```
3. instalar las dependencias
   ```
    pip3 install -r requirements.txt
   ```
4. ejecutar migraciones
    ```
    python3 manage.py migrate
    ```
5. ejecutar localmente:
    ```
    python3 manage.py runserver
    ```

### Test
Para ejecutar los test unitarios solo hay que posicionarse en la ruta /e-commerce/ y ejecutar:
```
python3 manage.py test
```

## Con docker 
Tener instalado docker y docker-compose.

1. Ejecutar desde la ruta principal del proyecto (e-commerce):
```
docker-compose up
```

## Uso

La aplicación utiliza SQLite como base de datos y no se proporciona un fichero con datos desde el inicio por lo que para poder crear productos y reviews es necesario primero crear como mínimo a un usuario administrador de los que proporciona Django.

Para crear usuarios usar el comando que proporciona Django ya que no hay un formulario implementado para ello, también se podría desde el panel admin pero al menos un superuser debe de crearse primero con este comando:
```
python3 manage.py createsuperuser
```
Solo hay que rellenar los datos que vaya pidiendo.

Una vez tengamos un usuario administrador se podrán crear más usando la ruta /admin/ ya que permite por defecto el CRUD de los modelos. (Producto y Review también han sido incluidos).

# Notas
Se pueden realizar más test unitarios aunque no los he desarrollado porque no dispongo de mucho tiempo, igual el no subirlo a una máquina de AWS.