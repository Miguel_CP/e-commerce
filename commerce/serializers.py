from rest_framework import serializers
from .models import Product, Review


class ReviewSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = Review
        fields = ['id', 'user', 'title', 'description', 'grade']
        read_only_fields = ['user', 'product']

    def validate(self, data):
        user = self.context['request'].user
        product = self.context['product']

        if Review.objects.filter(user=user, product=product).exists():
            raise serializers.ValidationError('Ya has creado una review para este producto.')


class ProductSerializer(serializers.ModelSerializer):
    reviews = ReviewSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = ['id', 'title', 'description', 'price', 'reviews']
