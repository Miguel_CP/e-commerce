from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from .models import Product


class ProductListCreateViewTest(APITestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.client.force_authenticate(user=self.user)

    def test_get_all_products(self):
        response = self.client.get(reverse('product_list_create'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), Product.objects.count())

    def test_post_create_product(self):
        expected_product = Product(title='title', description='description', price=9.99)
        data = {'title': expected_product.title, 'description': expected_product.description, 'price':expected_product.price}
        response = self.client.post(reverse('product_list_create'), data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.get().title, expected_product.title)
        self.assertEqual(Product.objects.get().description, expected_product.description)
        self.assertEqual(str(Product.objects.get().price), str(expected_product.price))

    def test_post_create_product_unauthenticated(self):
        self.client.force_authenticate(user=None)
        data = {'title': 'product title', 'description': 'product description', 'price': 9.99}
        response = self.client.post(reverse('product_list_create'), data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)



class ProductDetailViewTest(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.client.force_authenticate(user=self.user)
        self.product = Product.objects.create(title='Test Product', description='This is a test product.', price=9.99)

    def test_get_product_detail(self):
        url = reverse('product_detail', kwargs={'pk': self.product.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], 'Test Product')
        self.assertEqual(response.data['description'], 'This is a test product.')
        self.assertEqual(response.data['price'], '9.99')

    def test_get_product_detail_unauthenticated(self):
        self.client.force_authenticate(user=None)
        url = reverse('product_detail', kwargs={'pk': self.product.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], 'Test Product')
        self.assertEqual(response.data['description'], 'This is a test product.')
        self.assertEqual(response.data['price'], '9.99')
