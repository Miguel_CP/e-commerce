from django.urls import path

from .views import ProductListCreateView, ProductDetailView, ReviewListCreateView


urlpatterns = [
    path('products/', ProductListCreateView.as_view(), name='product_list_create'),
    path('products/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('products/<int:product_id>/reviews/', ReviewListCreateView.as_view(), name='review_list_create'),
]
